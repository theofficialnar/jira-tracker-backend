var express = require('express');
var router = express.Router();
const JiraApi = require('jira').JiraApi;

const jira = new JiraApi('https', 'jira.egalacoral.com', null, 'lunar.cuenca', 'Cu@ng092212', '2', false, false);

/**
 * Fetch tickets from JIRA REST API
 * @returns {json} tickets or error message 
 */
router.get('/', async (req, res) => {
  let jql = 'project = CRE AND status in (new, "in progress", reopened) AND assignee in (lunar.cuenca)';
  await jira.searchJira(jql, ["key", "summary", "duedate", "assignee", "status", "customfield_14801"], (err, data) => {
    if (data) {
      res.status(200).json({
        tickets: data.issues
      })
    }

    if (err) {
      res.status(404).json({
        err
      })
    }
  })
});

module.exports = router;
